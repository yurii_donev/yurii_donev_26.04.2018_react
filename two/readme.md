# Github API Navigator

The page in this task is a simple and naive navigator through Github API. Please
do the following:

- rewrite this page using React.js and ES6/7
- set up a build tool of your choice to transpile and bundle your JS code
- make any improvements/optimizations to the code/functionality you see fit
